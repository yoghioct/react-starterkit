import "./styles.css";
import { Login } from "./pages/login";
import { Daftar } from "./pages/daftar";
import { Main } from "./pages/main";
import { Dashboard } from "./pages/dashboard";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import ProtectedRoute from "./protectedRoutes";
import { useSelector } from "react-redux";

export default function App() {
  const auth = useSelector((state) => state.isAuthenticated);

  return (
    <Router>
      <Switch>
        <ProtectedRoute auth={auth} exact path="/dashboard" component={Dashboard} />
        <Route exact path="/login" component={Login} />
        <Route exact path="/daftar" component={Daftar} />
        <Route exact path="/" component={Main} />
      </Switch>
    </Router>
  );
}
