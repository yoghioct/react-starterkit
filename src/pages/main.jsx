import React from "react";
import { Container } from "react-bootstrap";
import { Card, Button } from "react-bootstrap";
import { Link } from 'react-router-dom';
import { Header } from "../components/header";

export const Main = () => {

  return (
    <>
    <Header />
    <Container>
      <Card className="card-login-utama text-center">
        <Card.Img variant="top" src={require("../assets/img/logo/logo-kotak.png")} />
        <Card.Body >
          <Card.Title>
            <h1 className="card-title">
              Selamat datang <br />
              di LMS-SIAP Bintang Pelajar
            </h1>
          </Card.Title>
          <Card.Text className="card-text">
              Untuk Siswa, silahkan log in menggunakan username dan password
          </Card.Text>
          <Card.Text>
              Untuk Orang tua, silahkan log in menggunakan akun Email dengan
              mengklik tombol Log in Orang tua Menggunakan Email yang terdaftar.
          </Card.Text>
          <Card.Text className="mt-9">
              Masuk sebagai
          </Card.Text>
            <Link to="/login">
                <Button className="button-main">Siswa</Button>
            </Link>
            <Link to="/login">
                <Button className="button-secondary">Orang Tua</Button>
            </Link>
        </Card.Body>
      </Card>
    </Container>
    </>
  );
};