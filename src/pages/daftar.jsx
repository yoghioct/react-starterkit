import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { Card, Button, Form, InputGroup, Row, Col } from "react-bootstrap";
import { useSelector, useDispatch } from "react-redux";
import { getLogin } from "../redux/auth/actions";
import { Header } from "../components/header";

export const Daftar = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const state = useSelector((state) => state.user);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  // const navigate = useNavigate();
  const handleSubmit = (e) => {
    e.preventDefault();
    if (email && password) {
      if (state.email === email && state.pass === password) {
        dispatch(getLogin());
        history.push("/dashboard");
      } else {
        alert("Credentials did not match");
      }
    } else {
      alert("Wrong Credentials");
    }
  };

  return (
    <>
    <Header />
      <Card className="card-login">
        <Card.Img
          variant="top"
          src={require("../assets/img/logo/logo-panjang.png")}
          className="center"
          style={{ width: "250px", margin: "10px auto" }}
        />
        <Card.Body>
          <Card.Title className="text-center">
            <h4>Selamat datang di LMS-SIAP Siswa</h4>
          </Card.Title>

          <Form onSubmit={handleSubmit}>
          <Form.Label>Nama Pengguna</Form.Label>
            <InputGroup className="mb-3">
              <InputGroup.Text id="basic-addon1">@</InputGroup.Text>
              <Form.Control
                type="text"
                placeholder="nama pengguna"
                value={text}
                onChange={(e) => setUsername(e.target.value)}
              />
            </InputGroup>
            <Form.Label>Email</Form.Label>
            <InputGroup className="mb-3">
              <InputGroup.Text id="basic-addon1">@</InputGroup.Text>
              <Form.Control
                type="email"
                placeholder="Enter email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </InputGroup>
            <Form.Label>Nama Lengkap</Form.Label>
            <InputGroup className="mb-3">
              <InputGroup.Text id="basic-addon1">@</InputGroup.Text>
              <Form.Control
                type="text"
                placeholder="nama lengkap"
                value={text}php
                onChange={(e) => setUsername(e.target.value)}
              />
            </InputGroup>
            <Form.Label>Kata Sandi</Form.Label>
            <InputGroup className="mb-3">
              <InputGroup.Text id="basic-addon1">@</InputGroup.Text>
              <Form.Control
                 type="password"
                 placeholder="Password"
                 value={password}
                 onChange={(e) => setPassword(e.target.value)}
                />
            </InputGroup> 

            <Form.Group className="mb-3" controlId="formBasicCheckbox">
              <Row>
                <Col>
                <Form.Check type="checkbox" label="ingat saya" />
                </Col>
                <Col>
                <a href="#"><p style={{float: 'right'}}>Lupa Kata Sandi</p></a>
                </Col>                
              </Row>
            </Form.Group>
            <Button className="button-signin" type="submit">
              Masuk
            </Button>
          </Form>
          <Card.Text className="text-center">atau masuk dengan</Card.Text>
          <div className="text-center row-social">
              <img
                src={require("../assets/img/social/facebook.png")}
                className="social-login"
                alt="facebook"
              />
              <img
                src={require("../assets/img/social/google.png")}
                className="social-login"
                alt="google"
              />
              <img
                src={require("../assets/img/social/microsoft.png")}
                className="social-login"
                alt="microsoft"
              />
            
          </div>
          <Card.Text className="text-center">
            2022<br></br>
            LMS-SIAP BINTANG PELAJAR
          </Card.Text>
        </Card.Body>
      </Card>
    </>
  );
};
