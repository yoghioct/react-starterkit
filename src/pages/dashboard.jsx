import { Headerauth } from "../components/header";

export const Dashboard = () => {
  return (
    <section>
      <Headerauth />
      <div
        className="container ht-100 
      d-flex justify-content-center 
      align-items-center"
      >
        <h2>Dashboard Page</h2>
      </div>
    </section>
  );
};
