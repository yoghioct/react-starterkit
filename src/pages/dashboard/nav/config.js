// component
import SvgColor from '../../../components/svg-color';

// ----------------------------------------------------------------------

const icon = (name) => <SvgColor src={`/assets/icons/navbar/${name}.svg`} sx={{ width: 1, height: 1 }} />;

const navConfig = [
  {
    title: 'dashboard',
    path: '/dashboard/app',
    icon: icon('ic_lock'),
  },
  {
    title: 'Kalender Pendidikan',
    path: '/dashboard/user',
    icon: icon('ic_lock'),
  },
  {
    title: 'Target Presensi',
    path: '/dashboard/products',
    icon: icon('ic_lock'),
  },
  {
    title: 'Bahan Belajar',
    path: '/dashboard/blog',
    icon: icon('ic_lock'),
  },
  {
    title: 'Ujian Online',
    path: '/login',
    icon: icon('ic_lock'),
  },
  {
    title: 'Lebih Banyak',
    path: '/404',
    icon: icon('ic_lock'),
  },
];

export default navConfig;
