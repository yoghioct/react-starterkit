import { useDispatch } from "react-redux";
import Navbar from "react-bootstrap/Navbar";
import { Link } from "react-router-dom";

export default (props = { ...props }) => {
  const dispatch = useDispatch();
  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <div className="container">
        <Navbar.Brand href="/">
          <img
            src={require("../../assets/img/logo/LMSSIAP.png")}
            width="auto"
            height="60px"
            className="d-inline-block align-top"
            alt="Logo LMS SIAP"
          />
        </Navbar.Brand>
        <Link to="/daftar">
          <button type="button" className="btn btn-success">
            Daftar
          </button>
        </Link>
      </div>
    </nav>
  );
};
