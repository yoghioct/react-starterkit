import { useDispatch } from "react-redux";
import { getLogout } from "../../redux/auth/actions";
import Navbar from 'react-bootstrap/Navbar';

export default  (props = {...props}) => {
  const dispatch = useDispatch();
    return (
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <div className="container">
      <Navbar.Brand href="#home">
            <img
              src={require("../../assets/img/logo/LMSSIAP.png")}
              width="auto"
              height="60px"
              className="d-inline-block align-top"
              alt="Logo LMS SIAP"
            />
          </Navbar.Brand>
        <button
          type="button"
          className="btn btn-danger"
          onClick={() => dispatch(getLogout())}
        >
          Log Out
        </button>
      </div>
    </nav>
    )
}