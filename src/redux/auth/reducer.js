import { LOGIN_SUCCESS, LOGOUT_SUCCESS } from "./constants";

const initialState = {
  isAuthenticated: localStorage.getItem("isAuth") || false,
  user: { email: "yoghi@gmail.com", pass: "asdf" }
};

// Reducers
const AuthReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN_SUCCESS:
      localStorage.setItem("isAuth", true);
      return {
        ...state,
        isAuthenticated: true,
      };

    case LOGOUT_SUCCESS:
      localStorage.removeItem("isAuth", false);
      return {
        ...state,
        isAuthenticated: false
      };

    default:
      return state;
  }
};

export default AuthReducer;
